class CreateGroupMembers < ActiveRecord::Migration
  def change
    create_table :group_members do |t|
      t.references :project_group, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
