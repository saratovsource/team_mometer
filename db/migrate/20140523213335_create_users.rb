class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.index  :email, unique: true
      t.string :state
      t.string :password_digest
      t.string :confirmation_token
      t.string :reset_password_token

      t.references :company, index: true

      t.timestamps
    end
  end
end
