class CreateProjectGroups < ActiveRecord::Migration
  def change
    create_table :project_groups do |t|
      t.string :name
      t.references :company, index: true

      t.timestamps
    end
  end
end
