Rails.application.routes.draw do
  if Rails.env.development?
    mount RegistrationMailView => 'registration_mail_view'
  end
  # Api
  namespace :api do
  end

  # Web
  scope module: :web do
    root to: "welcome#index"
    resources :companies, only: [:new, :create]
    resources :users,      only: [] do
      get :confirm, on: :member
    end
  end
end
