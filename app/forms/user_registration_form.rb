class UserRegistrationForm < ApplicationForm
  property :email
  property :name
  property :password
  property :password_confirmation

  validates :email, presence: true, email: true
  validates :name, presence: true
  validates_uniqueness_of :email
  validates :password, presence: true, confirmation: true
  validates :password_confirmation, presence: true
end
