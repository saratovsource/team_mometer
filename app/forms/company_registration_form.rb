class CompanyRegistrationForm < ApplicationForm
  model :company
  property :name
  validates :name, presence: true

  collection :users, form: UserRegistrationForm
end
