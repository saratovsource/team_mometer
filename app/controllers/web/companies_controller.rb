class Web::CompaniesController < Web::ApplicationController
  def new
    respond_with company_form
  end

  def create
    if company_form.validate(params[:company])
      company_form.save
      flash!(:success)
    end
    respond_with company_form, location: root_path
  end

  protected

  def company_form
    new_company = Company.new(users: [moderator])
    @company_form ||= CompanyRegistrationForm.new(new_company)
  end

  def moderator
    @user ||= User.new.tap do |u|
      u.add_role :moderator
    end
  end

end
