class Web::UsersController < Web::ApplicationController
  def confirm
    @user = User.disabled.where(confirmation_token: params[:id]).first
    if @user && @user.confirm
      flash!(:success)
      sign_in(@user)
    else
      flash!(:error)
    end
    redirect_to root_path
  end
end
