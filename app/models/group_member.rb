class GroupMember < ActiveRecord::Base
  belongs_to :project_group
  belongs_to :user
end
