class ProjectGroup < ActiveRecord::Base
  belongs_to :company
  has_many :group_members
  has_many :users, through: :group_members
end
