# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :project_group do
    name { generate :group_name }
  end
end
