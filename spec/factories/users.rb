# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name { generate :name }
    email { generate :email }

    password { generate :password }
    password_confirmation { password }

    factory :authenticated_user, aliases: [:interviewer] do
      company {|a| a.association :company }
      state :enabled
    end

    factory :requested_user do
      company {|a| a.association :company }
      confirmation_token { generate :confirmation_token }
    end
  end
end
