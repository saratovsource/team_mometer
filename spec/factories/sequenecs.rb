FactoryGirl.define do
  # Names
  sequence( :name ) { |n| Faker::NameMX.full_name }
  sequence( :company_name ) { |n| Faker::CompanyIT.name }
  sequence( :group_name ) { |n| "group-#{n}" }

  sequence( :email ) { |n| [n, Faker::Internet.email ].join('-') }

  sequence( :password, aliases: [:confirmation_token] ) {|n| SecureRandom.urlsafe_base64 }
end
