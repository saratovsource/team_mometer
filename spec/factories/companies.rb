# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company do
    name { generate :company_name }

    factory :new_company do
      users_attributes {|a| {
        0 => attributes_for( :user )
      }}
    end
  end
end
