require 'spec_helper'

describe Web::UsersController do
  describe "confirm new user" do
    let(:requested_user)     { create :requested_user }
    let(:confirmation_token) { requested_user.confirmation_token }
    it "confirm new user by confirmation token" do
      get :confirm, id: confirmation_token
      expect(response).to be_redirect
      expect(current_user).to eq(requested_user)
      expect(current_user.confirmation_token).to be_blank
    end
  end
end
