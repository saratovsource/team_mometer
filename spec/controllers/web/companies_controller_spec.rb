require 'spec_helper'

describe Web::CompaniesController do
  describe ".new" do
    it "show registration form" do
      get :new
      expect(response).to be_success
    end
  end

  describe ".create" do
    let( :company_attributes ){ attributes_for( :new_company ) }
    let( :company ){ assigns( :company_form ).model }
    let( :owner ){ company.users.first }
    it "creates new company with owner" do
      post :create, company: company_attributes
      expect(response).to be_redirect
      expect(company).to be_present
      expect(company.users).to be_any
      expect(owner.has_role?(:moderator)).to be_true
    end
  end
end
